# README

### 1. What is this?

A simple lib to abstract the loading of dynamic libraries.

### 2. Status

First implementation still in progress.

### 3. Future goals

Provide a simple interface for finding and loading lib binaries.

### 4. Usage (dev)

To be defined.
